﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfBaseRepository<Customer>, IRepository<Customer>
    {

        public CustomerRepository(Data.SqlLiteDbContext context) : base(context)
        {
        }
        public override async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await Context.Customers
                .Include(x => x.Preferences)
                .AsNoTrackingWithIdentityResolution()
                .ToListAsync();
        }
        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            return await Context.Customers
                .Include(x => x.CustomerPreferences)
                .Include(x => x.Preferences)
                .Include(x => x.PromoCodes)
                .AsNoTrackingWithIdentityResolution()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public override async Task<Customer> CreateAsync(Customer item)
        {
            var entityEntry = await Context.Customers.AddAsync(item);
            foreach(var preference in item.Preferences)
            {
                Context.Entry(preference).State = EntityState.Modified;
            }
            await Context.SaveChangesAsync();
            foreach (var preference in item.Preferences)
            {
                Context.Entry(preference).State = EntityState.Detached;
            }
            Context.Entry(entityEntry.Entity).State = EntityState.Detached;
            return entityEntry.Entity;
        }

        public override async Task<Customer> UpdateAsync(Customer item)
        {
            var customerPreferences = await Context.CustomerPreferences.Where(x => x.CustomerId == item.Id).ToListAsync();
            // Удаляем не актальные предпочтения:
            foreach (var customerPreference in customerPreferences.Where(x => !item.Preferences.Select(y => y.Id).Contains(x.PreferenceId)))
            {
                Context.Set<CustomerPreference>().Remove(customerPreference);
            }

            // Добавляем новые предпочтения:
            foreach (var preferenceId in item.Preferences.Select(x => x.Id).Where(x => !customerPreferences.Select(y => y.PreferenceId).Contains(x)))
            {
                await Context.Set<CustomerPreference>().AddAsync(new CustomerPreference()
                {
                    CustomerId = item.Id,
                    PreferenceId = preferenceId,
                });
            }

            item.Preferences = null;

            Context.Entry(item).State = EntityState.Modified;
            await Context.SaveChangesAsync();
            Context.Entry(item).State = EntityState.Detached;
            return await GetByIdAsync(item.Id);
        }
        public override async Task DeleteAsync(Guid id)
        {
            var item = await Context.Customers
                 .Include(x => x.PromoCodes)
                 .AsNoTrackingWithIdentityResolution()
                 .FirstOrDefaultAsync(x => x.Id == id);
            foreach(var promoCode in item.PromoCodes)
            {
                Context.PromoCodes.Remove(promoCode);
            }
            Context.Customers.Remove(item);
            await Context.SaveChangesAsync();
        }
    }
}