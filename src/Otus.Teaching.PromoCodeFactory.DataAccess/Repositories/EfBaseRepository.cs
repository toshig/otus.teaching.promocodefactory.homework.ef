﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfBaseRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        public readonly Data.SqlLiteDbContext Context;

        public EfBaseRepository(Data.SqlLiteDbContext Context_)
        {
            Context = Context_;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Context
                .Set<T>()
                .AsNoTrackingWithIdentityResolution()
                .ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await Context
                .Set<T>()
                .AsNoTrackingWithIdentityResolution()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public virtual async Task<T> CreateAsync(T item)
        {
            var entityEntry = await Context.Set<T>().AddAsync(item);
            await Context.SaveChangesAsync();
            return entityEntry.Entity;
        }
        public virtual async Task<T> UpdateAsync(T item)
        {
            Context.Entry(item).State = EntityState.Modified;
            await Context.SaveChangesAsync();
            Context.Entry(item).State = EntityState.Detached;
            return item;
        }
        public virtual async Task DeleteAsync(Guid id)
        {
            T item = await Context
                .Set<T>()
                .FirstOrDefaultAsync(x => x.Id == id);

            Context.Set<T>().Remove(item);
            await Context.SaveChangesAsync();
        }
    }
}