﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T newItem)
        {
            var temp = Data.ToList();
            temp.Add(newItem);
            Data = temp;
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == newItem.Id));
        }
        public Task<T> UpdateAsync(T updateItem)
        {
            var temps = Data.ToList();
            var temp = temps.FirstOrDefault(x => x.Id == updateItem.Id);
            temp = updateItem;
            Data = temps;
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == updateItem.Id));
        }
        public Task DeleteAsync(Guid id)
        {
            var temp = Data.ToList().Where(x => x.Id != id);
            return Task.FromResult(Data = temp);
        }
    }
}