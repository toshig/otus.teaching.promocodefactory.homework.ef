﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class SqlLiteDbContext : DbContext
    {        
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public SqlLiteDbContext(DbContextOptions<SqlLiteDbContext> dbContextOptions)
            : base(dbContextOptions)
        {
            //для 8 задания:
            //Database.EnsureDeleted(); 
            //Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasMany(p => p.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity<CustomerPreference>(
                    j => j
                        .HasOne(pt => pt.Preference)
                        .WithMany(t => t.CustomerPreferences)
                        .HasForeignKey(pt => pt.PreferenceId),
                    j => j
                        .HasOne(pt => pt.Customer)
                        .WithMany(p => p.CustomerPreferences)
                        .HasForeignKey(pt => pt.CustomerId)
                        );

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
        }
    }
}
