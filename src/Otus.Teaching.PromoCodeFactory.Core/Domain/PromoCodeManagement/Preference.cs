﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(100)]
        public string Name { get; set; }

        public DateTime CreateDateTime { get; set; } // Для 8 пункта задания

        public List<Customer> Customers { get; set; }
        public List<CustomerPreference> CustomerPreferences { get; set; }
    }
}