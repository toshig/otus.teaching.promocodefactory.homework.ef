﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName                    
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить данные клиентов по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName ,
                Preferences = customer.Preferences?.Select(x => new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                PromoCodes = customer.PromoCodes?.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList()
            };

            return customerModel;
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest newCustomer)
        {
            var newItem = new Customer()
            {
                Id = Guid.NewGuid(),
                Email = newCustomer.Email,
                FirstName = newCustomer.FirstName,
                LastName = newCustomer.LastName,
                Preferences = (await _preferenceRepository.GetAllAsync()).Where(x => newCustomer.PreferenceIds.Contains(x.Id)).ToList()
            };
            var customer = await _customerRepository.CreateAsync(newItem);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences?.Select(x => new PreferenceResponse()
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToList(),
                PromoCodes = customer.PromoCodes?.Select(x => new PromoCodeShortResponse()
                    {
                        Id = x.Id,
                        Code = x.Code,
                        BeginDate = x.BeginDate.ToString(),
                        EndDate = x.EndDate.ToString(),
                        PartnerName = x.PartnerName,
                        ServiceInfo = x.ServiceInfo
                    }).ToList()                
            };

            return customerModel;
        }

        /// <summary>
        /// Изменить клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<CustomerResponse>> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest updateCustomer)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var newItem = new Customer()
            {
                Id = id,
                Email = updateCustomer.Email,
                Preferences = (await _preferenceRepository.GetAllAsync()).Where(x => updateCustomer.PreferenceIds?.Contains(x.Id) ?? false).ToList(),
                FirstName = updateCustomer.FirstName,
                LastName = updateCustomer.LastName
            };

            customer = await _customerRepository.UpdateAsync(newItem);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.Preferences?.Select(x => new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList(),
                PromoCodes = customer.PromoCodes?.Select(x => new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo
                }).ToList()
            };

            return customerModel;
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            await _customerRepository.DeleteAsync(id);

            return Ok();
        }
    }
}